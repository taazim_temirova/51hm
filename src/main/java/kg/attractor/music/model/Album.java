package kg.attractor.music.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collation = "albums")
@Data
public class Album {
    @Id
    @Getter
    @Setter
    private String id;
    @Getter
    @Setter
    @Indexed
    private int year;
    @Getter
    @Setter
    @Indexed
    private String name;
    @DBRef
    @Getter
    @Setter
    private List<Author> authorList = new ArrayList<>();
    @DBRef
    @Getter
    @Setter
    private  List<Composition> compositionList = new ArrayList<>();

}
