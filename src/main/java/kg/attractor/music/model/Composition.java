package kg.attractor.music.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collation = "compositions")
@Data
public class Composition {
    @Id
    @Getter
    @Setter
    private String id;
    @Indexed
    @Getter
    @Setter
    private String name;
    @DBRef
    @Getter
    @Setter
    private List<Album> albums = new ArrayList<>();
    @DBRef
    @Getter
    @Setter
    private  List<Author> authors = new ArrayList<>();

}
