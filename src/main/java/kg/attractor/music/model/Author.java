package kg.attractor.music.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@CompoundIndex()
@Document(collection  = "authors")
@Data
public class Author {
    @Id
    @Getter
    @Setter
    @Indexed
    private String id;
    @Getter
    @Setter
    @Indexed
    private String name;
    @Getter
    @Setter
    @Indexed
    private  String surname;
    @DBRef
    @Getter
    @Setter
    private List<Album> albumList = new ArrayList<>();
}
